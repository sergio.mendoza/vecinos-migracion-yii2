create database vecinos360_yii2;
/* CREATE TABLE secuencia y comunicaciones */
CREATE SEQUENCE public.comunicaciones_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 3
  CACHE 1;
ALTER TABLE public.comunicaciones_id_seq
  OWNER TO postgres;

CREATE TABLE public.comunicaciones
(
  id_comunic integer NOT NULL DEFAULT nextval('comunicaciones_id_seq'::regclass),
  ip_modificacion character varying(50) NOT NULL,
  fecha_registro timestamp without time zone NOT NULL,
  fecha_modificacion timestamp without time zone NOT NULL,
  nom_comunidad character varying(50),
  asunto_comunic text,
  estado_comunic character varying(1),
  data jsonb NOT NULL,
  CONSTRAINT id_comunic PRIMARY KEY (id_comunic)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.comunicaciones
  OWNER TO postgres;
/* FIN CREATE TABLE secuencia y comunicaciones */

/* CREATE TABLE secuencia y tabla_migraciones */
CREATE SEQUENCE public.tablas_migracion_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 3
  CACHE 1;
ALTER TABLE public.tablas_migracion_id_seq
  OWNER TO postgres;
CREATE TABLE public.tablas_migracion
(
  id_tbl integer NOT NULL DEFAULT nextval('tablas_migracion_id_seq'::regclass),
  nom_tbl character varying(50) NOT NULL,
  estado_tbl smallint NOT NULL DEFAULT '0'::smallint, -- 1: Activo, 0:Inactivo
  fecha_registro timestamp without time zone NOT NULL,
  CONSTRAINT id_tbl PRIMARY KEY (id_tbl)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.tablas_migracion
  OWNER TO postgres;
COMMENT ON COLUMN public.tablas_migracion.estado_tbl IS '1: Activo, 0:Inactivo';

INSERT INTO tablas_migracion (nom_tbl,estado_tbl,fecha_registro)
VALUES ('blogestruct',1,'2017-09-06 10:58:44');
/* FIN CREATE TABLE secuencia y tabla_migraciones */

CREATE SEQUENCE public.id_tarea_frecuente_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 5
  CACHE 1;
ALTER TABLE public.id_tarea_frecuente_seq
  OWNER TO postgres;

CREATE TABLE public.tarea_frecuente
(
  id_tarea_frecuente integer NOT NULL DEFAULT nextval('id_tarea_frecuente_seq'::regclass),
  fecha_registro timestamp without time zone NULL,
  id_comunidad int not null,
  periodo character varying(5),
  tipo_final character varying(1) null,
  fecha_final timestamp without time zone NULL,
  estado character varying(1) null,
  CONSTRAINT id_tarea_frecuente PRIMARY KEY (id_tarea_frecuente)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.tarea_frecuente
  OWNER TO postgres;
COMMENT ON COLUMN public.tarea_frecuente.periodo IS 'd=diario, s=semanal, 1=mensual, 2=Bimestral, 3=Trimestral, 6=Semestral, 12=Anual';
COMMENT ON COLUMN public.tarea_frecuente.tipo_final IS '1=fecha, 0=nunca';

CREATE SEQUENCE public.id_tarea_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 5
  CACHE 1;
ALTER TABLE public.id_tarea_seq
  OWNER TO postgres;

CREATE TABLE public.tareas
(
  id_tarea integer NOT NULL DEFAULT nextval('id_tarea_seq'::regclass),
  id_tarea_frecuente integer null,
  tiposoli integer null,
  feclec timestamp without time zone NULL,
  fecini timestamp without time zone NULL,
  fecesti timestamp without time zone NULL,
  fecreal timestamp without time zone NULL,
  comentario text null,
  responsable integer null,
  imagen character varying(255),
  imagen_thumb character varying(255),
  id_usu_res integer null,
  observacion text null,
  usuario integer null,
  comunidad integer null,
  post text null,
  documento character varying(255),
  importante text null,
  comentarios integer null,
  tiposolicitud integer null,
  requerimiento character varying(1),
  envio_a text null,
  publico character varying(1) null,
  estado character varying(1) null,
  vistas_usuarios_id text null,
  id_mensaje integer null,
  origen character varying(1) null,
  CONSTRAINT id_tarea PRIMARY KEY (id_tarea)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.tareas
  OWNER TO postgres;
COMMENT ON COLUMN public.tareas.imagen IS 'Imagen sustento de ejecución de tarea';
COMMENT ON COLUMN public.tareas.id_usu_res IS 'Usuario respuesta';
COMMENT ON COLUMN public.tareas.importante IS 'json con los username que marcan como importante';
COMMENT ON COLUMN public.tareas.envio_a IS 'json de los username a los que se les envia el msj';
COMMENT ON COLUMN public.tareas.origen IS '1: Web, 2: App';

CREATE SEQUENCE public.id_act_comp_mantenimientos_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 5
  CACHE 1;
ALTER TABLE public.id_act_comp_mantenimientos_seq
  OWNER TO postgres;
 CREATE TABLE public.act_comp_mantenimientos
(
  id_act_comp_mantenimientos integer NOT NULL DEFAULT nextval('id_act_comp_mantenimientos_seq'::regclass),
  fec_registro timestamp without time zone NULL,
  id_activo integer null,
  id_componente integer null,
  id_tarea integer null,
  responsable character varying(100),
  observaciones text null,
  id_proveedor integer null,
  fec_instalacion timestamp without time zone NULL,
  costo decimal(10,2) NULL,
  garantia float NULL,
  duracion float NULL,
  nuevo character varying(1) null,
  CONSTRAINT id_act_comp_mantenimientos PRIMARY KEY (id_act_comp_mantenimientos)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.tarea_frecuente
  OWNER TO postgres;
COMMENT ON COLUMN public.act_comp_mantenimientos.nuevo IS '1=nuevo';

INSERT INTO tablas_migracion (nom_tbl,estado_tbl,fecha_registro, descripcion)
VALUES ('tareas',1,'2017-09-18 10:58:44','Se migran las tablas: id_mantenimiento,inner join solicitudes y blogestruct,tarea_frecuente');