<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "act_comp_mantenimientos".
 *
 * @property integer $id_act_comp_mantenimientos
 * @property string $fec_registro
 * @property integer $id_activo
 * @property integer $id_componente
 * @property integer $id_tarea
 * @property string $responsable
 * @property string $observaciones
 * @property integer $id_proveedor
 * @property string $fec_instalacion
 * @property string $costo
 * @property double $garantia
 * @property double $duracion
 * @property string $nuevo
 */
class ActCompMantenimientos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'act_comp_mantenimientos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fec_registro', 'fec_instalacion'], 'safe'],
            [['id_activo', 'id_componente', 'id_tarea', 'id_proveedor'], 'integer'],
            [['observaciones'], 'string'],
            [['costo', 'garantia', 'duracion'], 'number'],
            [['responsable'], 'string', 'max' => 100],
            [['nuevo'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_act_comp_mantenimientos' => 'Id Act Comp Mantenimientos',
            'fec_registro' => 'Fec Registro',
            'id_activo' => 'Id Activo',
            'id_componente' => 'Id Componente',
            'id_tarea' => 'Id Tarea',
            'responsable' => 'Responsable',
            'observaciones' => 'Observaciones',
            'id_proveedor' => 'Id Proveedor',
            'fec_instalacion' => 'Fec Instalacion',
            'costo' => 'Costo',
            'garantia' => 'Garantia',
            'duracion' => 'Duracion',
            'nuevo' => 'Nuevo',
        ];
    }
}
