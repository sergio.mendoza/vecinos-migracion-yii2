<?php

namespace app\models;

use Yii;
use yii\db\Query;
/**
 * This is the model class for table "blogestruct".
 *
 * @property integer $id
 * @property string $tipoblog
 * @property string $observacion
 * @property string $fechora
 * @property integer $usuario
 * @property integer $comunidad
 * @property string $perfilsesion
 * @property string $post
 * @property string $imagen
 * @property string $imagen_thumb
 * @property string $documento
 * @property string $importante
 * @property integer $comentarios
 * @property integer $tiposolicitud
 * @property string $requerimiento
 * @property string $envio_a
 * @property integer $publico
 * @property integer $estado
 * @property string $vistas_usuarios
 * @property string $vistas_usuarios_id
 * @property integer $id_mensaje
 * @property integer $origen
 */
class Blogestruct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blogestruct';
    }
    
    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db1'); 
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipoblog', 'fechora', 'usuario', 'comunidad', 'perfilsesion', 'post', 'imagen', 'imagen_thumb', 'documento', 'importante', 'comentarios', 'tiposolicitud', 'requerimiento'], 'required'],
            [['observacion', 'post', 'importante', 'envio_a', 'vistas_usuarios', 'vistas_usuarios_id'], 'string'],
            [['fechora'], 'safe'],
            [['usuario', 'comunidad', 'comentarios', 'tiposolicitud', 'publico', 'estado', 'id_mensaje', 'origen'], 'integer'],
            [['tipoblog', 'perfilsesion'], 'string', 'max' => 2],
            [['imagen', 'imagen_thumb', 'documento'], 'string', 'max' => 255],
            [['requerimiento'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tipoblog' => 'Tipoblog',
            'observacion' => 'Observacion',
            'fechora' => 'Fechora',
            'usuario' => 'Usuario',
            'comunidad' => 'Comunidad',
            'perfilsesion' => 'Perfilsesion',
            'post' => 'Post',
            'imagen' => 'Imagen',
            'imagen_thumb' => 'Imagen Thumb',
            'documento' => 'Documento',
            'importante' => 'Importante',
            'comentarios' => 'Comentarios',
            'tiposolicitud' => 'Tiposolicitud',
            'requerimiento' => 'Requerimiento',
            'envio_a' => 'Envio A',
            'publico' => 'Publico',
            'estado' => 'Estado',
            'vistas_usuarios' => 'Vistas Usuarios',
            'vistas_usuarios_id' => 'Vistas Usuarios ID',
            'id_mensaje' => 'Id Mensaje',
            'origen' => 'Origen',
        ];
    }
    
    public function getList(array $datos = array()){
        $query = new Query();
        $query->select(['b.*',"CONCAT_WS(' ',SUBSTRING_INDEX(r.nombres,' ',1),r.ape_paterno) AS author","c.nombre AS nomcomunidad", 
            "(CASE WHEN b.id = b.id_mensaje THEN 'PADRE' ELSE 'HIJO' END) AS RangoMensaje","CONCAT(s.nombre_seccion,' - ',d.departamento) AS dpto","u.id_usuario AS cod_usuario"])
                ->from(["blogestruct b"])
                ->innerJoin('tipoblog tb','b.tipoblog=tb.tipo')
                ->innerJoin('usuario_residente as ur','b.usuario=ur.id_usu_res')
                ->innerJoin('usuarios u','ur.id_usuario=u.id_usuario')
                ->innerJoin('residentes r','ur.id_residente=r.id_residente')
                ->innerJoin('comunidad c','c.id_comunidad = b.comunidad')
                ->leftJoin('departamento d','d.id_depa=r.id_depa')
                ->leftJoin('seccion s','s.id_seccion=d.id_seccion')
                ->where("b.tipoblog = 'ci'")
                ->andWhere("b.estado = '1'")
                ->andWhere("(CASE WHEN b.id = b.id_mensaje THEN 'PADRE' ELSE 'HIJO' END) = '".$datos['RangoMensaje']."'");
        if($datos['RangoMensaje'] == 'HIJO'){
            $query->andWhere("b.id_mensaje = '".$datos['id']."'");
        }
        if($datos['RangoMensaje'] == 'PADRE'){
            $query->orderBy('b.id ASC')->limit($datos['limit'])->offset($datos['offset']);
        }else{
           $query->orderBy('cast(b.fechora as datetime)'); 
        }
        $rows = $query->all($this->getDb());
        return $rows;
    }
    
    public function getListComunicaciones($limit,$offset){
        $data_padre = array('RangoMensaje' => 'PADRE','limit'=>$limit,'offset'=>$offset);
        $data_hijo = array('RangoMensaje' => 'HIJO');
        $arr_padre = $this->getList($data_padre);
        $arr_data_p = $arr_data_pd = $arr_data_h = $arr_data_hd = array();
        $i=0;
        foreach($arr_padre as $value_p){
            $arr_data_p[$i]['fecha_registro'] = $value_p['fechora'];
            $arr_data_p[$i]['fecha_modificacion'] = $value_p['fechora'];
            $arr_data_p[$i]['asunto_comunic'] = pg_escape_string(str_replace("'",'"',$value_p['post']));
            $arr_data_p[$i]['comunidad'] = pg_escape_string(str_replace("'",'"',$value_p['nomcomunidad']));
            $arr_data_pd['id_usuario'] = $value_p['cod_usuario'];
            $arr_data_pd['id_usu_res'] = $value_p['usuario'];
            $arr_data_pd['fecha_envio'] = $value_p['fechora'];
            $arr_data_pd['publico'] = $value_p['publico'];
            $arr_data_pd['estado'] = '1';//$value_p['estado'];
            $arr_data_pd['id_comunidad'] = $value_p['comunidad'];
            $arr_data_pd['origen'] = $value_p['origen'];
            $data_doc = empty(trim($value_p['documento'])) ? array() : json_decode($value_p['documento'],true);
            $arr_data_pd['adjuntos'] = $data_doc;
            $arr_data_pd['cuerpo_comunic'] = pg_escape_string($value_p['observacion']);
            $data_correo = empty(trim($value_p['envio_a'])) ? array() : json_decode($value_p['envio_a'],true);
            $arr_data_pd['envio_a'] = $data_correo;
            $arr_data_pd['vistas_usuarios_id'] =  explode(',', $value_p['vistas_usuarios_id']);
            $arr_data_pd['flag'] = $value_p['RangoMensaje'].'-'.$value_p['id'];
            $arr_data_pd['author'] = $value_p['author'];
            $importante = empty(trim($value_p['importante'])) ? array() : json_decode($value_p['importante'],true);
            $arr_data_pd['importante'] = $importante;
            $arr_data_pd['dpto'] = is_null($value_p['dpto'])?'':pg_escape_string(str_replace("'",'"',$value_p['dpto']));
            $arr_data_p[$i]['data'][] = $arr_data_pd;
            $data_hijo['id'] = $value_p['id'];
            $arr_hijo = $this->getList($data_hijo);
            if(count($arr_hijo) > 0){
                $j=0;
                foreach ($arr_hijo as $value_h){
                    $arr_data_hd['id_usuario'] = $value_h['cod_usuario'];
                    $arr_data_hd['id_usu_res'] = $value_h['usuario'];
                    $arr_data_hd['id_usuario'] = $value_h['usuario'];
                    $arr_data_hd['fecha_envio'] = $value_h['fechora'];
                    $arr_data_hd['publico'] = $value_h['publico'];
                    $arr_data_hd['estado'] = '1';//$value_h['estado'];
                    $arr_data_hd['id_comunidad'] = $value_h['comunidad'];
                    $arr_data_hd['origen'] = $value_h['origen'];
                    $data_doc1 = empty(trim($value_h['documento'])) ? array() : json_decode($value_h['documento'],true);
                    $arr_data_hd['adjuntos'] = $data_doc1;
                    $arr_data_hd['cuerpo_comunic'] = pg_escape_string(str_replace("'",'"',$value_h['observacion']));
                    $data_correo1 = empty(trim($value_h['envio_a'])) ? array() : json_decode($value_h['envio_a'],true);
                    $arr_data_hd['envio_a'] = $data_correo1;
                    $arr_data_hd['vistas_usuarios_id'] = explode(',', $value_h['vistas_usuarios_id']);
                    $arr_data_hd['flag'] = $value_h['RangoMensaje'].'-'.$value_h['id_mensaje'];
                    $arr_data_hd['author'] = pg_escape_string(str_replace("'",'"',$value_h['author']));
                    $importante1 = empty(trim($value_h['importante'])) ? array() : json_decode($value_h['importante'],true);
                    $arr_data_hd['importante'] = $importante1;
                    $arr_data_hd['dpto'] = is_null($value_h['dpto'])?'':pg_escape_string(str_replace("'",'"',$value_h['dpto']));
                    $arr_data_p[$i]['data'][] = $arr_data_hd;
                    $j++;
                }
                $arr_data_p[$i]['fecha_modificacion'] = $arr_hijo[$j-1]['fechora'];
            }
            $i++;
        }
        return $arr_data_p;
    }
}
