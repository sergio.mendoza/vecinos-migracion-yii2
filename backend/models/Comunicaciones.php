<?php

namespace app\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "comunicaciones".
 *
 * @property integer $id_comunic
 * @property string $token_usu
 * @property string $fecha_registro
 * @property integer $id_comunidad
 * @property string $asunto_comunic
 * @property string $descripcion_comunic
 * @property string $data
 */
class Comunicaciones extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comunicaciones';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_comunic', 'token_usu', 'fecha_registro', 'id_comunidad', 'data'], 'required'],
            [['id_comunic', 'id_comunidad'], 'integer'],
            [['fecha_registro'], 'safe'],
            [['data'], 'string'],
            [['token_usu'], 'string', 'max' => 50],
            [['asunto_comunic', 'descripcion_comunic'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_comunic' => 'Id Comunic',
            'token_usu' => 'Token Usu',
            'fecha_registro' => 'Fecha Registro',
            'id_comunidad' => 'Id Comunidad',
            'asunto_comunic' => 'Asunto Comunic',
            'descripcion_comunic' => 'Descripcion Comunic',
            'data' => 'Data',
        ];
    }
    
    public function getListRecibidos(array $datos = array()){
        $query = new Query();
        $query->select(['c.*',"jsonb_array_length(c.data) as cantmsj"])
                ->from(["comunicaciones c"]);
        $estad = $datos['estado'];
        $estado = '[{"estado":"'.$estad.'"}]';
        $user = $datos['username'];
        $envio_a = '[{"envio_a":["'.$user.'"]}]';
        //FILTRO TODOS
        $query->where("c.data @> ANY (ARRAY['".$estado."']::jsonb[])")
              ->andWhere("c.data @> ANY (ARRAY['".$envio_a."']::jsonb[])");
        switch (trim($datos['filtro'])):
            case 'f'://FECHA
                $query->andWhere("CAST(c.fecha_modificacion AS TIMESTAMP) BETWEEN '{$datos['uno']}' AND '{$datos['dos']}'");
                break;
            case 'cm'://COMUNIDAD
                $params = $datos['uno'];
                $params1 = '[{"id_comunidad":"'.$params.'"}]';
                $query->andWhere("(c.data @> ANY (ARRAY['".$params1."']::jsonb[]))");
                break;
            case 'r'://REMITENTE
                $params1 = strtolower($datos['uno']);
                $query->andWhere("LOWER(CAST(c.data->(JSONB_ARRAY_LENGTH(c.data) -1)->>'author' AS TEXT)) LIKE '%{$params1}%'");
                break;
        endswitch;
        $query->orderBy('CAST(c.fecha_modificacion as TIMESTAMP) DESC')
                ->limit($datos['limit'])->offset($datos['offset']);
        //echo $query->createCommand()->getRawSql();exit;
        $rows = $query->all();
        return $rows;
    }
    
    public function getListEnviados(array $datos = array()){
        $query = new Query();
        $query->select(['c.*',"jsonb_array_length(c.data) as cantmsj"])
                ->from(["comunicaciones c"]);
        $estad = $datos['estado'];
        $estado = '[{"estado":"'.$estad.'"}]';
        $id_usuario = $datos['id_usuario'];
        $envio_a = '[{"id_usuario":"'.$id_usuario.'"}]';
        //FILTRO TODOS
        $query->where("c.data @> ANY (ARRAY['".$estado."']::jsonb[])")
              ->andWhere("c.data @> ANY (ARRAY['".$envio_a."']::jsonb[])");
        switch (trim($datos['filtro'])):
            case 'f'://FECHA
                $query->andWhere("CAST(c.fecha_modificacion AS TIMESTAMP) BETWEEN '{$datos['uno']}' AND '{$datos['dos']}'");
                break;
            case 'cm'://COMUNIDAD
                $params = $datos['uno'];
                $params1 = '[{"id_comunidad":"'.$params.'"}]';
                $query->andWhere("(c.data @> ANY (ARRAY['".$params1."']::jsonb[]))");
                break;
            case 'r'://REMITENTE
                $params1 = strtolower($datos['uno']);
                $query->andWhere("LOWER(CAST(c.data->(JSONB_ARRAY_LENGTH(c.data) -1)->>'author' AS TEXT)) LIKE '%{$params1}%'");
                break;
        endswitch;
        $query->orderBy('CAST(c.fecha_modificacion as TIMESTAMP) DESC')
                ->limit($datos['limit'])->offset($datos['offset']);
        //echo $query->createCommand()->getRawSql();exit;
        $rows = $query->all();
        return $rows;
    }
    
    public function updateImportante(array $datos = array()){
        $arr_usu_res = $arr_data_seteada = array();
        $posicion = $datos['posicion'];
        $id_comunic = $datos['id_comunic'];
        $id_usu_res = $datos['id_usu_res'];
        $i = $datos['i'];
        $data = $this->obtenerComunicacionesPorId($datos);
        $arr_comunic = json_decode($data[0]['data'],true);
        $arr_importante = $arr_comunic[$posicion]['importante'];
        if(count($arr_importante)>0){
            if($i){//eliminar al usuario
                $key = array_search($id_usu_res, $arr_importante);
                unset($arr_importante[$key]);
            }else{
                array_push($arr_importante, $id_usu_res);
            }
            $arr_comunic[$posicion]['importante'] = $arr_importante;
            $json_data_seteada = json_encode($arr_comunic[$posicion]);
        }else{
            $arr_usu_res[] = $id_usu_res;
            $arr_comunic[$posicion]['importante'] = $arr_usu_res;
            $json_data_seteada = json_encode($arr_comunic[$posicion]);
        }
        $sql = "UPDATE comunicaciones SET data = jsonb_set(data, '{".$posicion."}', '".$json_data_seteada."'::jsonb) WHERE id_comunic = '{$id_comunic}'";
        $respuesta = Yii::$app->db->createCommand($sql)->execute();
        return array('respuesta'=>$respuesta);
    }
    
    public function obtenerComunicacionesPorId(array $datos = array()){
        $query = new Query();
        $query->select(['c.*'])
                ->from(["comunicaciones c"])
                ->where("c.id_comunic = '{$datos['id_comunic']}'");
        $rows = $query->all();
        return $rows;
    }
    
    public function insertarComunicaciones(array $datos = array()){
        $arr_data_msj = array();
        $ip = $datos['ip'];
        $fecha_registro = $datos['fechora'];
        $fecha_modificacion = $datos['fechora'];
        $nom_comunidad = isset($datos['nom_comunidad'])?pg_escape_string(str_replace("'",'"',$datos['nom_comunidad'])):'';
        $asunto_comunic = isset($datos['titulo'])?pg_escape_string(str_replace("'",'"',$datos['titulo'])):'';
        $mensaje_cuerpo = isset($datos['mensaje_cuerpo'])?pg_escape_string(str_replace("'",'"',$datos['mensaje_cuerpo'])):'';
        $envio_a = $datos['envio_a'];
        $estado_comunic = '1';
        $arr_envio_a = json_decode($envio_a,true);
        $arr_data_msj[0]['id_usuario'] = $datos['id_usuario'];
        $arr_data_msj[0]['id_usu_res'] = $datos['id_usu_res'];
        $arr_data_msj[0]['fecha_envio'] = $datos['fechora'];
        $arr_data_msj[0]['publico'] = '1';
        $arr_data_msj[0]['estado'] = '1';
        $arr_data_msj[0]['id_comunidad'] = $datos['id_com'];
        $arr_data_msj[0]['origen'] = $datos['origen'];
        $arr_data_msj[0]['adjuntos'] = empty(trim($datos['documento'])) ? array() : json_decode($datos['documento'],true);
        $arr_data_msj[0]['cuerpo_comunic'] = $mensaje_cuerpo;
        $arr_data_msj[0]['envio_a'] = $arr_envio_a;
        $arr_data_msj[0]['vistas_usuarios_id'] = array();
        $arr_data_msj[0]['flag'] = 'PADRE';
        $arr_data_msj[0]['author'] = $datos['author'];
        $arr_data_msj[0]['importante'] = array();
        $arr_data_msj[0]['dpto'] = $datos['dpto'];
        $data_json = json_encode($arr_data_msj);
        $sql = "INSERT INTO comunicaciones (ip_modificacion,fecha_registro,fecha_modificacion,nom_comunidad,asunto_comunic,estado_comunic,data) VALUES"
                . "('".$ip."','".$fecha_registro."','".$fecha_modificacion."','".$nom_comunidad."','".$asunto_comunic."','".$estado_comunic."','".$data_json."')";
        $respuesta = Yii::$app->db->createCommand($sql)->execute();
        return $respuesta;
    }
}
