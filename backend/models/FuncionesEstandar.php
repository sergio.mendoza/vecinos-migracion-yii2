<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models;

/**
 * Description of FuncionesEstandar
 *
 * @author sergio
 */
class FuncionesEstandar {

    public $msj;

    public function __construct() {
       $this->msj = '';
    }

    public function obtener_ip_cliente(){
        $ip_address = "";
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_address .= $_SERVER['HTTP_X_FORWARDED_FOR'] . ": ";
        }
        $ip_address .= $_SERVER['REMOTE_ADDR'];
        return $ip_address;
    }
}
