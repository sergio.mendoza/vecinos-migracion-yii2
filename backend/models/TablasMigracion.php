<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tablas_migracion".
 *
 * @property integer $id_tbl
 * @property string $nom_tbl
 * @property integer $estado_tbl
 * @property string $fecha_registro
 */
class TablasMigracion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tablas_migracion';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nom_tbl', 'fecha_registro'], 'required'],
            [['estado_tbl'], 'integer'],
            [['fecha_registro'], 'safe'],
            [['nom_tbl'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_tbl' => 'Id Tbl',
            'nom_tbl' => 'Nom Tbl',
            'estado_tbl' => 'Estado Tbl',
            'fecha_registro' => 'Fecha Registro',
        ];
    }
}
