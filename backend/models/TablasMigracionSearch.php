<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TablasMigracion;

/**
 * TablasMigracionSearch represents the model behind the search form about `app\models\TablasMigracion`.
 */
class TablasMigracionSearch extends TablasMigracion
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['estado_tbl','id_tbl'], 'integer'],
            [['fecha_registro'], 'safe'],
            [['nom_tbl'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TablasMigracion::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_tbl' => $this->id_tbl,
            'nom_tbl' => $this->nom_tbl,
            'estado_tbl' => $this->estado_tbl,
            'fecha_registro' => $this->fecha_registro,
        ]);

//        $query->andFilterWhere(['like', 'tipoblog', $this->tipoblog])
//            ->andFilterWhere(['like', 'perfilsesion', $this->perfilsesion])
//            ->andFilterWhere(['like', 'observacion', $this->observacion])
//            ->andFilterWhere(['like', 'envio_a', $this->envio_a])
//            ->andFilterWhere(['like', 'post', $this->post]);

        return $dataProvider;
    }
}
