<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tarea_frecuente".
 *
 * @property integer $id_tarea_frecuente
 * @property string $fecha_registro
 * @property integer $id_comunidad
 * @property string $periodo
 * @property string $tipo_final
 * @property string $fecha_final
 * @property string $estado
 */
class TareaFrecuente extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tarea_frecuente';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fecha_registro', 'fecha_final'], 'safe'],
            [['id_comunidad'], 'required'],
            [['id_comunidad'], 'integer'],
            [['periodo'], 'string', 'max' => 5],
            [['tipo_final', 'estado'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_tarea_frecuente' => 'Id Tarea Frecuente',
            'fecha_registro' => 'Fecha Registro',
            'id_comunidad' => 'Id Comunidad',
            'periodo' => 'Periodo',
            'tipo_final' => 'Tipo Final',
            'fecha_final' => 'Fecha Final',
            'estado' => 'Estado',
        ];
    }
}
