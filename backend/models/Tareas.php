<?php

namespace app\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "tareas".
 *
 * @property integer $id_tarea
 * @property integer $id_tarea_frecuente
 * @property integer $tiposoli
 * @property string $feclec
 * @property string $fecini
 * @property string $fecesti
 * @property string $fecreal
 * @property string $comentario
 * @property integer $responsable
 * @property string $imagen
 * @property string $imagen_thumb
 * @property integer $id_usu_res
 * @property string $observacion
 * @property integer $usuario
 * @property integer $comunidad
 * @property string $post
 * @property string $documento
 * @property string $importante
 * @property integer $comentarios
 * @property integer $tiposolicitud
 * @property string $requerimiento
 * @property string $envio_a
 * @property string $publico
 * @property string $estado
 * @property string $vistas_usuarios_id
 * @property integer $id_mensaje
 * @property string $origen
 */
class Tareas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tareas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_tarea_frecuente', 'tiposoli', 'responsable', 'id_usu_res', 'usuario', 'comunidad', 'comentarios', 'tiposolicitud', 'id_mensaje'], 'integer'],
            [['feclec', 'fecini', 'fecesti', 'fecreal'], 'safe'],
            [['comentario', 'observacion', 'post', 'importante', 'envio_a', 'vistas_usuarios_id'], 'string'],
            [['imagen', 'imagen_thumb', 'documento'], 'string', 'max' => 255],
            [['requerimiento', 'publico', 'estado', 'origen'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_tarea' => 'Id Tarea',
            'id_tarea_frecuente' => 'Id Tarea Frecuente',
            'tiposoli' => 'Tiposoli',
            'feclec' => 'Feclec',
            'fecini' => 'Fecini',
            'fecesti' => 'Fecesti',
            'fecreal' => 'Fecreal',
            'comentario' => 'Comentario',
            'responsable' => 'Responsable',
            'imagen' => 'Imagen',
            'imagen_thumb' => 'Imagen Thumb',
            'id_usu_res' => 'Id Usu Res',
            'observacion' => 'Observacion',
            'usuario' => 'Usuario',
            'comunidad' => 'Comunidad',
            'post' => 'Post',
            'documento' => 'Documento',
            'importante' => 'Importante',
            'comentarios' => 'Comentarios',
            'tiposolicitud' => 'Tiposolicitud',
            'requerimiento' => 'Requerimiento',
            'envio_a' => 'Envio A',
            'publico' => 'Publico',
            'estado' => 'Estado',
            'vistas_usuarios_id' => 'Vistas Usuarios ID',
            'id_mensaje' => 'Id Mensaje',
            'origen' => 'Origen',
        ];
    }
    
    public function obteniendoDataMysql(array $data = array()){
        //conexion a mysql
        $cn_mysql= Yii::$app->get('db1');
        $db_mysql_connection = new yii\db\Connection($cn_mysql);
        $sql = "";
        switch (trim($data['table'])):
            case 'tarea_frecuente':
                $sql .= "SELECT tf.id_tarea_frecuente,tf.fecha_registro,tf.id_comunidad,tf.periodo,tf.tipo_final,tf.fecha_final,".
                        "tf.estado FROM tarea_frecuente tf ORDER BY tf.id_tarea_frecuente ASC;";
                break;
            case 'solicitudes_blogestruct':
                $sql .= "SELECT s.idsoli,s.tiposoli,s.feclec,s.fecini,s.fecesti,s.fecreal,s.id_tarea_frecuente,s.comentario,s.responsable".
                        ",s.id_usu_res,b.observacion,b.usuario,b.comunidad,b.post,b.imagen,b.imagen_thumb,b.documento,b.importante,".
                        "b.comentarios ,b.tiposolicitud,b.requerimiento,b.envio_a,b.publico,b.estado,b.vistas_usuarios_id,b.id_mensaje,b.origen,s.es_solicitud,b.fechora,s.post_origen".
                        " FROM solicitudes s ".
                        "INNER JOIN blogestruct b on s.idpost = b.id ".
                        "WHERE b.estado != '0' ORDER BY s.idsoli ASC LIMIT ".$data['limit']." OFFSET ".$data['offset'].";";
                break;
            case 'act_comp_mantenimientos':
                $sql .= "SELECT acm.id_mantenimiento,acm.fec_registro,acm.id_activo,acm.id_componente,acm.idsoli,acm.responsable,".
                    "acm.observaciones,acm.id_proveedor,acm.fec_instalacion,acm.costo,acm.garantia,acm.duracion,acm.nuevo "
                    . "FROM act_comp_mantenimientos acm ORDER BY acm.id_mantenimiento ASC;";
                break;
        endswitch;
        $rows = $db_mysql_connection->createCommand($sql)->queryAll();
        return $rows;
    }

    public function procesoMigracionTareas1($tarea_frecuente){
        $arr_datos = $arr_id_tarea_frecuente = $arr_id_soli = array();
        $arr_datos['table'] = $tarea_frecuente;
        $data_tarea_frecuente = $this->obteniendoDataMysql($arr_datos);
        $registros_insertados1 = $registros_no_insertados1 = $cantResgistro1 = 0;
        $transaction1 = Yii::$app->get('db');
        foreach ($data_tarea_frecuente as $value1):
            
            if($value1['fecha_final'] == '0000-00-00'){
                $value1['fecha_final'] = '1000-10-10';
            }
            $sql_insert1 = "INSERT INTO tarea_frecuente (id_tarea_frecuente,fecha_registro,id_comunidad,periodo,tipo_final,fecha_final,estado)"
                ." VALUES ('".$value1['id_tarea_frecuente']."','".$value1['fecha_registro']."','".$value1['id_comunidad']."','".$value1['periodo']."','".$value1['tipo_final']."','".$value1['fecha_final']."','".$value1['estado']."');";
            $respuesta1 = $transaction1->createCommand($sql_insert1)->execute();
            if($respuesta1){
                $registros_insertados1++;
            }else{
                $registros_no_insertados1++;
            }
            $cantResgistro1++;
        endforeach;
        return array('registros_insertados'=>$registros_insertados1,'registros_no_insertados'=>$registros_no_insertados1,'cantResgistro'=> $cantResgistro1);
    }
    
    function procesoMigracionTareas2($solicitudes_blogestruct,$limit,$offset){
        $arr_datos = $arr_id_tarea_frecuente = $arr_id_soli = array();
        $arr_datos['table'] = $solicitudes_blogestruct;
        $arr_datos['limit'] = $limit;
        $arr_datos['offset'] = $offset;
        $data_tarea = $this->obteniendoDataMysql($arr_datos);
        $registros_insertados2 = $registros_no_insertados2 = $cantResgistro2 = 0;
        $transaction2 = Yii::$app->get('db');
        foreach ($data_tarea as $value2):
            $sql_insert2 = "INSERT INTO tareas (id_tarea,id_tarea_frecuente,tiposoli,feclec,fecini,fecesti,fecreal,comentario,"
                . "responsable,imagen,imagen_thumb,id_usu_res,observacion,usuario,comunidad,post,documento,importante,comentarios,"
                . "tiposolicitud,requerimiento,envio_a,publico,estado,vistas_usuarios_id,id_mensaje,origen,es_solicitud,fechora,post_origen) ";
            if(empty(trim($value2['id_tarea_frecuente']))){
                $value2['id_tarea_frecuente'] = '0';
            }
            if(trim($value2['feclec']) == '0000-00-00 00:00:00' || empty(trim($value2['feclec']))){
                $value2['feclec'] = '1000-10-10 00:00:00';
            }
            if(trim($value2['fecini']) == '0000-00-00 00:00:00' || empty(trim($value2['fecini']))){
                $value2['fecini'] = '1000-10-10 00:00:00';
            }
            if(trim($value2['fecesti']) == '0000-00-00 00:00:00' || empty(trim($value2['fecesti']))){
                $value2['fecesti'] = '1000-10-10 00:00:00';
            }
            if(trim($value2['fecreal']) == '0000-00-00 00:00:00' || empty(trim($value2['fecreal']))){
                $value2['fecreal'] = '1000-10-10 00:00:00';
            }
            if(empty(trim($value2['tiposoli']))){
                $value2['tiposoli'] = '0';
            }
            if(empty(trim($value2['responsable']))){
                $value2['responsable'] = '0';
            }
            if(empty(trim($value2['id_usu_res']))){
                $value2['id_usu_res'] = '0';
            }
            if(empty(trim($value2['usuario']))){
                $value2['usuario'] = '0';
            }
            if(empty(trim($value2['comunidad']))){
                $value2['comunidad'] = '0';
            }
            if(empty(trim($value2['tiposolicitud']))){
                $value2['tiposolicitud'] = '0';
            }
            if(empty(trim($value2['id_mensaje']))){
                $value2['id_mensaje'] = '0';
            }
            if(trim($value2['fechora']) == '0000-00-00 00:00:00' || empty(trim($value2['fechora']))){
                $value2['fechora'] = '1000-10-10 00:00:00';
            }
            if(empty(trim($value2['post_origen'])) || is_null($value2['post_origen'])){
                $value2['post_origen'] = '0';
            }
            $sql_insert2 .= "VALUES ('".$value2['idsoli']."','".$value2['id_tarea_frecuente']."','".$value2['tiposoli']."','".$value2['feclec']."','".$value2['fecini']."','".$value2['fecesti']."','".$value2['fecreal']."'"
                    . ",'".$value2['comentario']."','".$value2['responsable']."','".$value2['imagen']."','".$value2['imagen_thumb']."','".$value2['id_usu_res']."','".$value2['observacion']."','".$value2['usuario']."'"
                    . ",'".$value2['comunidad']."','".$value2['post']."','".$value2['documento']."','".$value2['importante']."','".$value2['comentarios']."','".$value2['tiposolicitud']."','".$value2['requerimiento']."','".$value2['envio_a']."','".$value2['publico']."','".$value2['estado']."','".$value2['vistas_usuarios_id']."','".$value2['id_mensaje']."'"
                    . ",'".$value2['origen']."','".$value2['es_solicitud']."','".$value2['fechora']."','".$value2['post_origen']."');";
            $respuesta2 = $transaction2->createCommand($sql_insert2)->execute();
            if($respuesta2){
                $registros_insertados2++;
            }else{
                $registros_no_insertados2++;
            }
            $cantResgistro2++;
        endforeach;
        return array('registros_insertados'=>$registros_insertados2,'registros_no_insertados'=>$registros_no_insertados2,'cantResgistro'=> $cantResgistro2);
    }
    
    public function procesoMigracionTareas3($act_comp_mantenimientos){
        $arr_datos = $arr_id_tarea_frecuente = $arr_id_soli = array();
        $arr_datos['table'] = $act_comp_mantenimientos;
        $data_act_comp_mantenimientos = $this->obteniendoDataMysql($arr_datos);
        $registros_insertados3 = $registros_no_insertados3 = $cantResgistro3 = 0;
        $transaction3 = Yii::$app->get('db');
        foreach ($data_act_comp_mantenimientos as $value3):
            if(trim($value3['fec_registro']) == '0000-00-00 00:00:00' || empty(trim($value3['fec_registro']))){
                $value3['fec_registro'] = '1000-10-10 00:00:00';
            }
            if(empty(trim($value3['id_activo']))){
                $value3['id_activo'] = '0';
            }
            if(empty(trim($value3['id_componente']))){
                $value3['id_componente'] = '0';
            }
            if(empty(trim($value3['idsoli']))){
                $value3['idsoli'] = '0';
            }
            if(empty(trim($value3['id_proveedor']))){
                $value3['id_proveedor'] = '0';
            }
            if(trim($value3['fec_instalacion']) == '0000-00-00 00:00:00' || trim($value3['fec_instalacion']) == '0000-00-00' || empty(trim($value3['fec_instalacion']))){
                $value3['fec_instalacion'] = '1000-10-10 00:00:00';
            }
            if(empty(trim($value3['costo']))){
                $value3['costo'] = '0.00';
            }
            if(empty(trim($value3['garantia']))){
                $value3['garantia'] = '0';
            }
            if(empty(trim($value3['duracion']))){
                $value3['duracion'] = '0';
            }
            $sql_insert3 = "INSERT INTO act_comp_mantenimientos (id_act_comp_mantenimientos,fec_registro,id_activo,id_componente,id_tarea,responsable,observaciones,id_proveedor,fec_instalacion,costo,garantia,duracion,nuevo) "
                    . "VALUES('".$value3['id_mantenimiento']."','".$value3['fec_registro']."','".$value3['id_activo']."'"
                    . ",'".$value3['id_componente']."','".$value3['idsoli']."','".$value3['responsable']."'"
                    . ",'".$value3['observaciones']."','".$value3['id_proveedor']."','".$value3['fec_instalacion']."'"
                    . ",'".$value3['costo']."','".$value3['garantia']."','".$value3['duracion']."','".$value3['nuevo']."');";
            $respuesta3 = $transaction3->createCommand($sql_insert3)->execute();
            if($respuesta3){
                $registros_insertados3++;
            }else{
                $registros_no_insertados3++;
            }
            $cantResgistro3++;
        endforeach;
        return array('registros_insertados'=>$registros_insertados3,'registros_no_insertados'=>$registros_no_insertados3,'cantResgistro'=> $cantResgistro3);
    }
    
    public function getListTareas(array $datos = array()){
        $sql = "SELECT * FROM(SELECT t.id_tarea,t.post,t.imagen,t.documento ,t.fechora,t.post_origen,t.es_solicitud,
                t.fecini,t.feclec, t.fecesti,t.fecreal,t.tiposoli,usuario,(CASE
                WHEN (t.es_solicitud = '1' AND (t.feclec='1000-10-10 00:00:00'))
                                THEN 2 /*por atender*/
                WHEN (t.fecesti!='1000-10-10 00:00:00') AND
                                (t.fecreal='1000-10-10 00:00:00') AND
                                (DATE_PART('second', CURRENT_TIMESTAMP - t.fecesti::timestamp) <= 0)
                                THEN 3 /*programada*/
                WHEN (t.fecesti!='1000-10-10 00:00:00') AND
                                ( t.fecreal='1000-10-10 00:00:00') AND
                                (DATE_PART('second', CURRENT_TIMESTAMP - t.fecesti::timestamp) > 0)
                                THEN 4 /*vencida*/
                WHEN (t.fecreal!='1000-10-10 00:00:00') AND
                (DATE_PART('second', t.fecesti - t.fecreal::timestamp) < 0)
                                THEN 5 /*Fuera de Fecha*/
                WHEN (t.fecreal!='1000-10-10 00:00:00') AND
                (DATE_PART('second', t.fecesti - t.fecreal::timestamp) >= 0)
                                THEN 6 /*terminada*/
                ELSE 3
                END) AS estado 
                FROM tareas t
                WHERE t.estado='1' and t.comunidad='".$datos['id_com']."' ".$datos['add_query_tipo_sol']." ".$datos['rango'] ." ORDER BY  estado ASC, fecini DESC, fecesti DESC ) as x ".$datos['filtro'];
        //echo $sql;exit;
        $rows = Yii::$app->db->createCommand($sql)->queryAll();
        return $rows;
    }
    
    public function getEliminarTareas(array $datos = array()){
        $fecha_modificacion = date('Y-m-d H:i:s');
        $condition = "id_tarea = '".$datos['id_soli']."' AND comunidad = '".$datos['id_com']."'";
        $respuesta = Yii::$app->db->createCommand()->update('tareas', ['estado' => '0','ip_modificacion' => $datos['ip']
                ,'fecha_modificacion' => $fecha_modificacion,'reg_estado' => '0'], $condition)->execute();
        return $respuesta;
    }
}