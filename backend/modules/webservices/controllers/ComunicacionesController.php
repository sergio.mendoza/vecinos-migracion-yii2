<?php

namespace backend\modules\webservices\controllers;

use Yii;
use yii\rest\ActiveController;
use app\models\Comunicaciones;
use app\models\FuncionesEstandar;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;

/**
 * ComunicacionesController implements the CRUD actions for User model.
 */
class ComunicacionesController extends Controller
{
    
    public function beforeAction($action) { 
       $this->enableCsrfValidation = false; 
       return parent::beforeAction($action);
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];
        return $behaviors;
    }


    public function actionMensaje($op,$limit,$offset,$username,$estado,$filtro,$uno='',$dos=''){
        $objComunicaciones = new Comunicaciones();
        $arr_datos = array('limit'=>$limit,'offset'=>$offset,'uno'=>$uno,'dos'=>$dos);
        $data = array();
        switch (trim($op)):
            case '1'://tipo ci = comunicacion interna RECIBIDOS
                $arr_datos['username'] = $username;
                $arr_datos['estado'] = $estado;
                $arr_datos['filtro'] = $filtro;
                $data = $objComunicaciones->getListRecibidos($arr_datos);
                break;
            case '2'://tipo ci = comunicacion interna ENVIADOS
                $arr_datos['id_usuario'] = $username;
                $arr_datos['estado'] = $estado;
                $arr_datos['filtro'] = $filtro;
                $data = $objComunicaciones->getListEnviados($arr_datos);
                break;
        endswitch;
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['data' => $data];
    }
    
    public function actionImportante($op,$id_comunic='',$posicion='',$i='',$id_usu_res=''){
        $objComunicaciones = new Comunicaciones();
        $arr_datos = array('id_comunic'=>$id_comunic,'i'=>$i,'posicion'=>$posicion,'id_usu_res'=>$id_usu_res);
        $data = array();
        switch (trim($op)):
            case '1':
                $data = $objComunicaciones->updateImportante($arr_datos);
                break;
            case '2':
                $data = $objComunicaciones->obtenerComunicacionesPorId($arr_datos);
                break;
        endswitch;
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['data' => $data];
    }
    
    public function actionCreatenew($op){
        $objComunicaciones = new Comunicaciones();
        $objFuncionesEstandar = new FuncionesEstandar();
        $ip = $objFuncionesEstandar->obtener_ip_cliente();
        $body = Yii::$app->request->getRawBody();
        $arr_body = json_decode($body,true);
        $data = array();
        switch (trim($op)):
            case '1':
                $arr_body['ip'] = $ip;
                $data = $objComunicaciones->insertarComunicaciones($arr_body);
                break;
        endswitch;
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['data' => $data];
    }

    /**
     * Finds the TablasMigracion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TablasMigracion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TablasMigracion::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
