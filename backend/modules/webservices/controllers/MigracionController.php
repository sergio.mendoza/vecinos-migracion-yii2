<?php

namespace backend\modules\webservices\controllers;

use Yii;
use app\models\TablasMigracion;
use app\models\TablasMigracionSearch;
use app\models\Blogestruct;
use app\models\FuncionesEstandar;
use app\models\Tareas;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class MigracionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TablasMigracion models.
     * @return mixed
     */
    public function actionIndex()
    {
        //Yii::$app->db1;
        $searchModel = new TablasMigracionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionProcesar($nom_tbl,$limit='',$offset=''){
        $blogestruct = new Blogestruct();
        $funcionesEstandar = new FuncionesEstandar();
        $objTareas = new Tareas();
        $cn_postgres = Yii::$app->get('db');
        $db_pg_connection = new yii\db\Connection($cn_postgres);
        switch ($nom_tbl):
            case 'blogestruct':
                $data = $blogestruct->getListComunicaciones($limit,$offset);
                $cantResgistro = $registros_insertados = $registros_no_insertados = 0;
                $ip = $funcionesEstandar->obtener_ip_cliente();
                foreach ($data as $value):
                    $ip_modificacion = $ip;
                    $fecha_registro = $value['fecha_registro'];
                    $fecha_modificacion = $value['fecha_modificacion'];
                    $nom_comunidad = pg_escape_string($value['comunidad']);
                    $asunto_comunic = pg_escape_string($value['asunto_comunic']);
                    $estado_comunic = '';
                    $data_json = json_encode($value['data']);
                    $sql_insert = '';
                    $sql_insert .= "INSERT INTO comunicaciones(ip_modificacion, fecha_registro,fecha_modificacion, nom_comunidad,asunto_comunic,estado_comunic, data) VALUES ";
                    $sql_insert .= "('".$ip_modificacion."','".$fecha_registro."','".$fecha_modificacion."','".$nom_comunidad."','".$asunto_comunic."','".$estado_comunic."','".$data_json."');";
                    $respuesta = $db_pg_connection->createCommand($sql_insert)->execute();
                    if($respuesta){
                        $registros_insertados++;
                    }else{
                        $registros_no_insertados++;
                    }
                    $cantResgistro++;
                endforeach;
                $tabla = 'comunicaciones';
                break;
            case 'tarea_frecuente':
                $data = $objTareas->procesoMigracionTareas1('tarea_frecuente');
                $cantResgistro = $data['cantResgistro'];
                $registros_insertados = $data['registros_insertados'];
                $registros_no_insertados = $data['registros_no_insertados'];
                $tabla = 'tarea_frecuente';
                break;
            case 'solicitudes_blogestruct':
                $data = $objTareas->procesoMigracionTareas2('solicitudes_blogestruct',$limit,$offset);
                $cantResgistro = $data['cantResgistro'];
                $registros_insertados = $data['registros_insertados'];
                $registros_no_insertados = $data['registros_no_insertados'];
                $tabla = 'solicitudes_blogestruct';
                break;
            case 'act_comp_mantenimientos':
                $data = $objTareas->procesoMigracionTareas3('act_comp_mantenimientos');
                $cantResgistro = $data['cantResgistro'];
                $registros_insertados = $data['registros_insertados'];
                $registros_no_insertados = $data['registros_no_insertados'];
                $tabla = 'act_comp_mantenimientos';
                break;
        endswitch;
        return $this->renderAjax('procesar', [
            'cantResgistro' => $cantResgistro, 'registros_insertados' =>$registros_insertados, 'tabla' => $tabla, 
            'registros_no_insertados' => $registros_no_insertados
        ]);
    }

    /**
     * Finds the TablasMigracion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TablasMigracion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TablasMigracion::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
