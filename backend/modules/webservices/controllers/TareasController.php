<?php

namespace backend\modules\webservices\controllers;

use Yii;
use yii\rest\ActiveController;
use app\models\Tareas;
use app\models\FuncionesEstandar;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;

/**
 * ComunicacionesController implements the CRUD actions for User model.
 */
class TareasController extends Controller
{
    
    public function beforeAction($action) { 
       $this->enableCsrfValidation = false; 
       return parent::beforeAction($action);
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];
        return $behaviors;
    }


    public function actionAdmin($op){
        $objTareas = new Tareas();
        $objFuncionesEstandar = new FuncionesEstandar();
        $arr_datos = array();
        $data = array();
        $body = Yii::$app->request->getRawBody();
        $arr_body = json_decode($body,true);
        $ip = $objFuncionesEstandar->obtener_ip_cliente();
        switch (trim($op)):
            case '1'://LISTAR TAREAS
                $arr_datos['id_com'] = $arr_body['id_com'];
                $arr_datos['rango'] = $arr_body['rango'];
                $arr_datos['filtro'] = $arr_body['filtro'];
                $arr_datos['add_query_tipo_sol'] = $arr_body['add_query_tipo_sol'];
                $data = $objTareas->getListTareas($arr_datos);
                break;
            case '2'://ELIMINAR TAREA
                $arr_datos['id_com'] = $arr_body['id_com'];
                $arr_datos['id_soli'] = $arr_body['id_soli'];
                $arr_datos['ip'] = $ip;
                $data = $objTareas->getEliminarTareas($arr_datos);
                break;
        endswitch;
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['data' => $data];
    }

    /**
     * Finds the TablasMigracion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TablasMigracion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TablasMigracion::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
