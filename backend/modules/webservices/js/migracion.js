$(function() {
    
});

function procesar(nom_tbl){
    $('#nom_tbl').val(nom_tbl);
    $('#modalForm').modal('show');
}

function procesar_data(){
    var offset = $('#offset').val();
    var nom_tbl = $('#nom_tbl').val();
    if(offset == ''){
        alert('Debe ingresar el offset.');
        return false;
    }
    $('#resultado_proceso').html('');
    $('#modalForm').modal('hide');
    $.ajax({
        url : "/webservices/migracion/procesar?nom_tbl="+nom_tbl+'&offset='+offset,
        type: "POST",
        dataType: "HTML",
        beforeSend: function(){
            $("#btn-procesar").attr("disabled",true);
            $("#btn-procesar").html('<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"> Procesando</span>');
        },
        data: {nom_tbl:nom_tbl},
        success: function(result){
            $("#btn-procesar").attr("disabled",false);
            $("#btn-procesar").html('<span class="glyphicon glyphicon-wrench"> <b>Procesar</b></span>');
            $('#resultado_proceso').html(result);
        },
        timeout:80000
    });
}


