<div class="migracion-procesar">
    <table id="table-result" class="table table-bordered table-hover">
        <tr class="breadcrumb">
            <td><center><b>Tabla</b></center></td>
            <td><center><b><?php echo $tabla;?></b></center></td>
        </tr>
        <tr>
            <td>Cantidad de insert generados</td>
            <td><?php echo $cantResgistro;?></td>
        </tr>
        <tr>
            <td><p class="text-success">Registros insertados.</p></td>
            <td><p class="text-success"><?php echo $registros_insertados;?></p></td>
        </tr>
        <tr>
            <td><p class="text-danger">Registros no insertados.</p></td>
            <td><p class="text-danger"><?php echo $registros_no_insertados;?></p></td>
        </tr>
    </table>
</div>
